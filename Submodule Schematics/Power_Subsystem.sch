EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small_US R3
U 1 1 622368B2
P 6950 4300
F 0 "R3" H 7018 4346 50  0000 L CNN
F 1 "100 ohm" H 7018 4255 50  0000 L CNN
F 2 "" H 6950 4300 50  0001 C CNN
F 3 "~" H 6950 4300 50  0001 C CNN
F 4 "$0.0134" H 6950 4300 50  0001 C CNN "Cost"
F 5 "KNP1WS-100ohm+-5%-xt52" H 6950 4300 50  0001 C CNN "JLCPCB partname"
	1    6950 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 62236EA8
P 6950 4900
F 0 "D2" V 6904 4979 50  0000 L CNN
F 1 "1.5" V 6995 4979 50  0000 L CNN
F 2 "" H 6950 4900 50  0001 C CNN
F 3 "~" H 6950 4900 50  0001 C CNN
F 4 "$0.0943" H 6950 4900 50  0001 C CNN "Cost"
F 5 "DDZ9678-7" H 6950 4900 50  0001 C CNN "JLCPCB partname"
	1    6950 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 5350 6950 5050
$Comp
L Transistor_FET:2N7000 Q2
U 1 1 6223EDCD
P 8350 4150
F 0 "Q2" V 8692 4150 50  0000 C CNN
F 1 "NTD5867NL" V 8600 4100 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 8550 4075 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 8350 4150 50  0001 L CNN
F 4 "$0.4175" H 8350 4150 50  0001 C CNN "Cost"
F 5 "NTD5867NL" H 8350 4150 50  0001 C CNN "JLCPCB partname"
	1    8350 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 4200 6950 4050
Wire Wire Line
	8350 4350 8350 4650
$Comp
L power:GNDREF #PWR0104
U 1 1 62241980
P 6950 5350
F 0 "#PWR0104" H 6950 5100 50  0001 C CNN
F 1 "GNDREF" H 6955 5177 50  0000 C CNN
F 2 "" H 6950 5350 50  0001 C CNN
F 3 "" H 6950 5350 50  0001 C CNN
	1    6950 5350
	1    0    0    -1  
$EndComp
Text Label 9000 4050 0    50   ~ 0
UnderVoltageProtectionOutput
Text Notes 8450 6900 0    79   ~ 0
Light Sensor Project
Text Notes 8300 7500 0    50   ~ 0
Power System
Text Notes 10700 7650 0    50   ~ 0
1
Text Notes 8500 7650 0    50   ~ 0
11 March 2022
Wire Wire Line
	2000 5450 2000 5800
$Comp
L Device:Battery Li-Ion1
U 1 1 6222FF3C
P 2950 4300
F 0 "Li-Ion1" V 2705 4300 50  0000 C CNN
F 1 "5V" V 2796 4300 50  0000 C CNN
F 2 "" V 2950 4360 50  0001 C CNN
F 3 "~" V 2950 4360 50  0001 C CNN
F 4 "$0.3372" H 2950 4300 50  0001 C CNN "Cost"
F 5 "CR1220" H 2950 4300 50  0001 C CNN "JLCPCB partname"
	1    2950 4300
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP1_Small C1
U 1 1 622A8A4C
P 2000 5900
F 0 "C1" H 2091 5946 50  0000 L CNN
F 1 "10uF" H 2091 5855 50  0000 L CNN
F 2 "" H 2000 5900 50  0001 C CNN
F 3 "~" H 2000 5900 50  0001 C CNN
F 4 "CA45-A-10V-10uF-k" H 2000 5900 50  0001 C CNN "JLCPCB partname"
F 5 "$0.1099" H 2000 5900 50  0001 C CNN "Cost"
	1    2000 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C2
U 1 1 622A9047
P 4000 5900
F 0 "C2" H 4091 5946 50  0000 L CNN
F 1 "100uF" H 4091 5855 50  0000 L CNN
F 2 "" H 4000 5900 50  0001 C CNN
F 3 "~" H 4000 5900 50  0001 C CNN
F 4 "CA45-B-10V-100uF-k" H 4000 5900 50  0001 C CNN "JLCPCB partname"
F 5 "$0.2273" H 4000 5900 50  0001 C CNN "Cost"
	1    4000 5900
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM1117-3.3 U1
U 1 1 622B29BC
P 2750 5450
F 0 "U1" H 2750 5692 50  0000 C CNN
F 1 "LM1117-3.3" H 2750 5601 50  0000 C CNN
F 2 "" H 2750 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 2750 5450 50  0001 C CNN
F 4 "XC6206P332MR" H 2750 5450 50  0001 C CNN "JLCPCB partname"
F 5 "$0.1445" H 2750 5450 50  0001 C CNN "Cost"
	1    2750 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5450 2450 5450
Wire Wire Line
	4000 5450 4000 5800
$Comp
L Device:R_Small_US R1
U 1 1 622B4E53
P 3300 5550
F 0 "R1" H 3368 5596 50  0000 L CNN
F 1 "1k" H 3368 5505 50  0000 L CNN
F 2 "" H 3300 5550 50  0001 C CNN
F 3 "~" H 3300 5550 50  0001 C CNN
F 4 "0603+-5%1K" H 3300 5550 50  0001 C CNN "JLCPCB partname"
F 5 "$0.0011" H 3300 5550 50  0001 C CNN "Cost"
	1    3300 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 622B52C8
P 3300 6000
F 0 "R2" H 3368 6046 50  0000 L CNN
F 1 "1.64k" H 3368 5955 50  0000 L CNN
F 2 "" H 3300 6000 50  0001 C CNN
F 3 "~" H 3300 6000 50  0001 C CNN
F 4 "AC0603FR-071K74L" H 3300 6000 50  0001 C CNN "JLCPCB partname"
F 5 "$0.0021" H 3300 6000 50  0001 C CNN "Cost"
	1    3300 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 5450 3300 5450
Connection ~ 3300 5450
Wire Wire Line
	3300 5450 4000 5450
Wire Wire Line
	3300 5650 3300 5850
Wire Wire Line
	2750 5750 2750 5850
Wire Wire Line
	2750 5850 3300 5850
Connection ~ 3300 5850
Wire Wire Line
	3300 5850 3300 5900
Wire Wire Line
	2000 6000 2000 6300
Wire Wire Line
	3300 6100 3300 6300
Wire Wire Line
	4000 6000 4000 6300
$Comp
L power:GNDREF #PWR?
U 1 1 622B79F8
P 2000 6300
F 0 "#PWR?" H 2000 6050 50  0001 C CNN
F 1 "GNDREF" H 2050 6150 50  0000 C CNN
F 2 "" H 2000 6300 50  0001 C CNN
F 3 "" H 2000 6300 50  0001 C CNN
	1    2000 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 622B7F4C
P 3300 6300
F 0 "#PWR?" H 3300 6050 50  0001 C CNN
F 1 "GNDREF" H 3350 6150 50  0000 C CNN
F 2 "" H 3300 6300 50  0001 C CNN
F 3 "" H 3300 6300 50  0001 C CNN
	1    3300 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 622B8272
P 4000 6300
F 0 "#PWR?" H 4000 6050 50  0001 C CNN
F 1 "GNDREF" H 4050 6150 50  0000 C CNN
F 2 "" H 4000 6300 50  0001 C CNN
F 3 "" H 4000 6300 50  0001 C CNN
	1    4000 6300
	1    0    0    -1  
$EndComp
Text Label 3400 4300 0    50   ~ 0
BatteryOutput
Connection ~ 2000 5450
$Comp
L Device:D_Zener D3
U 1 1 622BD324
P 4550 5450
F 0 "D3" H 4550 5234 50  0000 C CNN
F 1 "0.7" H 4550 5325 50  0000 C CNN
F 2 "" H 4550 5450 50  0001 C CNN
F 3 "~" H 4550 5450 50  0001 C CNN
F 4 "$0.0102" H 4550 5450 50  0001 C CNN "Cost"
F 5 "BZT0V7" H 4550 5450 50  0001 C CNN "JLCPCB partname"
	1    4550 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 5450 4400 5450
Connection ~ 4000 5450
Wire Wire Line
	4700 5450 4900 5450
$Comp
L Device:D_Zener D1
U 1 1 622BF851
P 2200 4300
F 0 "D1" H 2200 4084 50  0000 C CNN
F 1 "0.7" H 2200 4175 50  0000 C CNN
F 2 "" H 2200 4300 50  0001 C CNN
F 3 "~" H 2200 4300 50  0001 C CNN
F 4 "$0.0102" H 2200 4300 50  0001 C CNN "Cost"
F 5 "BZT0V7" H 2200 4300 50  0001 C CNN "JLCPCB partname"
	1    2200 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1750 4100 1750 4300
Wire Wire Line
	1750 4300 2050 4300
Wire Wire Line
	2350 4300 2750 4300
Wire Wire Line
	3150 4300 3400 4300
Wire Wire Line
	6950 4050 6550 4050
Text Label 6550 4050 0    50   ~ 0
BatteryOutput
Text Label 2000 5000 0    50   ~ 0
UnderVoltageProtectionOutput
Wire Wire Line
	8550 4050 9000 4050
Wire Wire Line
	2000 5000 2000 5450
Text Label 4900 5450 0    50   ~ 0
RegulatedOutput
$Comp
L power:+8V #PWR?
U 1 1 622D77A5
P 1750 4100
F 0 "#PWR?" H 1750 3950 50  0001 C CNN
F 1 "+8V" H 1765 4273 50  0000 C CNN
F 2 "" H 1750 4100 50  0001 C CNN
F 3 "" H 1750 4100 50  0001 C CNN
	1    1750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4400 6950 4650
Wire Wire Line
	6950 4050 8150 4050
Connection ~ 6950 4050
Wire Wire Line
	6950 4650 8350 4650
Connection ~ 6950 4650
Wire Wire Line
	6950 4650 6950 4750
Text Notes 8900 7500 0    50   ~ 0
Submodule
$EndSCHEMATC
